#include "blowfish_h.h"

unsigned long estimate_block_size(unsigned int len) {
    unsigned long largo = 0;
    if (len > 0)
    {
        largo = 1;
        while (len > largo)
        {
            largo *= 2;
        }
    }
    return largo;
}

char* hex2str(byte* hex, unsigned int len) {
    int i;
    unsigned long largo = len * 2;
    char *app;
    char *buf;
    app = (char *) calloc(1, sizeof (byte));
    buf = (char *) calloc(largo + 1, sizeof (byte));
    strcpy(buf, "");

    for (i = 0; i < largo; i++)
    {
        sprintf(app, "%02x", (byte) hex[i]);
        strcat(buf, app);
    }

    buf[largo] = '\0';
    free(app);

    return buf;
}

byte* str2hex(char *txt, unsigned int len) {
    byte* result = (byte *) calloc((len / 2) + 1, sizeof (byte));
    unsigned int i = 0;

    for (i = 0; i < (len / 2); i++) {
        sscanf(txt + 2 * i, "%02x", (unsigned int *) &result[i]);
        //printf("bytearray %d: %02x\n", i, bytearray[i]);
    }

    return result;
}

char* cifrar(char* key, char* msj) {
    // long largo = ((strlen(msj) / 8) + 1) * 8;
    unsigned long largo = estimate_block_size(strlen(msj));
    byte iv[8] = {8, 8, 8, 8, 8, 8, 8, 8};
    //byte* cifrado = (byte *) msj;

    BF_KEY clave;
    BF_set_key(&clave, strlen(key), (byte *) key);
    byte *txt = (byte *) calloc(largo, sizeof (byte));

    BF_cbc_encrypt((byte *) msj, txt, largo, &clave, iv, BF_ENCRYPT);

    return hex2str(txt, largo);
}

char* descifrar(char* key, char* msj) {
    char* txt;
    long largo = (strlen(msj) * 2);
    byte iv[8] = {8, 8, 8, 8, 8, 8, 8, 8};
    byte* cifrado = str2hex(msj, strlen(msj));

    BF_KEY clave;
    BF_set_key(&clave, strlen(key), (byte *) key);
    txt = (char *) calloc(largo + 1, sizeof (char));

    BF_cbc_encrypt(cifrado, (byte *) txt, largo, &clave, iv, BF_DECRYPT);
    free(cifrado);
    return txt;
}
